/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Alert,
  Modal,
  TouchableHighlight,
  TextInput,
  Switch,
  Picker,
  Slider,
  ToastAndroid,
  TouchableOpacity,
  Clipboard,
  Dimensions
} from 'react-native';

import ImagePicker from 'react-native-image-picker';

var options = {
  title: 'Select Avatar',
  customButtons: [
    {name: 'fb', title: 'Choose Photo from Facebook'},
  ],
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

export default class scrollView extends Component {
  
  constructor() {
    super();
    this.state = {
      width:0,
      height:0
    };
    this.showPicker();
  }

  showPicker(){
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source
        });
      }
    });
  }

  render() {
    return (
      <Image source={this.state.avatarSource} style={styles.uploadAvatar} />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  button: {
    width: 100,
    height: 100
  },
  scrollView: {
    backgroundColor: '#6A85B1',
    height: 300,
  },
  title: {
    fontWeight: '500',
  },
  uploadAvatar: {
    width:100,
    height:100
  }
});

AppRegistry.registerComponent('scrollView', () => scrollView);
